# Darkside prototype GUI analyzers

This repository contains two ROOT-based GUIs for analyzing Darkside prototype data.

* The main folder contains a GUI used for Proto-1T.
* The vx2740 folder contains a GUI that just display waveforms from the VX2740 digitizers. You must compile it separately from the main GUI.

Both GUIs require [ROOT](https://root.cern.ch/) and [rootana](https://bitbucket.org/tmidas/rootana) to be installed.

The VX2740 analyzer requires a version of rootana from 14 February 2021 or later! This is when support for 64-bit banks was added to rootana.