#ifndef TChannelHistograms_h
#define TChannelHistograms_h

#include <string>
#include "THistogramArrayBase.h"
#include "TSimpleHistogramCanvas.hxx"

/// Class for making histograms of waveform baselines
class TChannelHistograms : public THistogramArrayBase {
public:
  TChannelHistograms();

  void UpdateHistograms(TDataContainer& dataContainer);  // update histograms

  void CreateHistograms(); // create histograms
  
};


/// Class for making summary histogram of baselines
class TBaselineSummary : public THistogramArrayBase {
public:
  TBaselineSummary();

  void UpdateHistograms(TDataContainer& dataContainer); // update histograms

  void CreateHistograms(); // create histograms
  
  // Create a simple canvas out of this, since we only expect one.
  TCanvasHandleBase* CreateCanvas();
  
};


/// Class for making histograms of waveform RMS
class TChannelRMS : public THistogramArrayBase {
public:
  TChannelRMS();

  void UpdateHistograms(TDataContainer& dataContainer);  // update histograms

  void CreateHistograms(); // create histograms
  
};

/// Class for making histograms of waveform RMS
class TChannelROI : public THistogramArrayBase {
public:
  TChannelROI();

  void UpdateHistograms(TDataContainer& dataContainer);  // update histograms

  void CreateHistograms(); // create histograms
  
};


/// Class for making summary histogram of waveform RMS
class TRMSSummary : public THistogramArrayBase {
public:
  TRMSSummary();

  void UpdateHistograms(TDataContainer& dataContainer); // update histograms

  void CreateHistograms(); // create histograms
  
  // Create a simple canvas out of this, since we only expect one.
  TCanvasHandleBase* CreateCanvas();
  
};



#endif


