#include "TPulseHistograms.h"

#include "TDirectory.h"
#include "TH2D.h"
#include "TWaveformProcessor.hxx"


const int numberChannelPerModule = 16;


TPulseCharges::TPulseCharges(){

  SetTabName("Pulses");
  SetSubTabName("Pulse Charge ");
  SetNumberChannelsInGroup(numberChannelPerModule);
  CreateHistograms();

}

void TPulseCharges::CreateHistograms(){

  // check if we already have histogramss.
  if(size() != 0){
    char tname[100];
    sprintf(tname,"PulseCharge_0_0");
    
    TH1D *tmp = (TH1D*)gDirectory->Get(tname);
    if (tmp) return;
  }

  // Otherwise make histograms
  clear();

  for(int j = 0; j < 4; j++){ // loop over modules
    for(int i = 0; i < numberChannelPerModule; i++){ // loop over 16 channels
      
      char name[100];
      char title[100];
      sprintf(name,"PulseCharge_%i_%i",j,i);
      TH1D *otmp = (TH1D*)gDirectory->Get(name);
      if (otmp) delete otmp;      
 
      sprintf(title,"Pulse Charge module=%i, channel=%i",j,i);
      
      TH1D *tmp = new TH1D(name, title, 5000, 0,500000);
      tmp->SetXTitle("Pulse Charge (ADC*sample value)");
      tmp->SetYTitle("Counts");
      
      push_back(tmp);
    }
  }
}

void TPulseCharges::UpdateHistograms(TDataContainer& dataContainer){

  // Grab the data from the singleton
  std::vector<TDSPulse> pulses = dswp::instance()->GetPulses();
  for(unsigned int i = 0; i < pulses.size(); i++){
    TDSPulse pulse = pulses[i];
    GetHistogram(pulse.index)->Fill(pulse.charge);
  }
}


TPulseChargeSummary::TPulseChargeSummary(){

  SetTabName("Pulses");
  SetSubTabName("Pulse Charge Summary");
  CreateHistograms();
}

void TPulseChargeSummary::CreateHistograms(){
  char name[100];
  sprintf(name,"PulseChargeSummary");

  // check if we already have histogramss.
  if(size() != 0){

    TH1D *tmp = (TH1D*)gDirectory->Get(name);
    if (tmp) return;
  }

  // Otherwise make histograms
  clear();

  char title[100];
  sprintf(title,"Pulse Charge Summary");

  TH2D *tmp = new TH2D(name, title, 5000, 0,500000, 4*numberChannelPerModule, 0, 4*numberChannelPerModule);
  tmp->SetXTitle("Pulse Charge (ADC*sample value)");
  tmp->SetYTitle("Channel");

  push_back(tmp);
}

void TPulseChargeSummary::UpdateHistograms(TDataContainer& dataContainer){

  // Grab the data from the singleton
  std::vector<TDSPulse> pulses = dswp::instance()->GetPulses();
  for(unsigned int i = 0; i < pulses.size(); i++){
    TDSPulse pulse = pulses[i];
    GetHistogram(0)->Fill(pulse.charge, pulse.index);
  }
}

TCanvasHandleBase* TPulseChargeSummary::CreateCanvas(){
  return new TSimpleHistogramCanvas(GetHistogram(0),"Pulse Charge Summary", "COLZ");
}



TPulseHeights::TPulseHeights(){

  SetTabName("Pulses");
  SetSubTabName("Pulse Heights");
  SetNumberChannelsInGroup(numberChannelPerModule);
  CreateHistograms();

}


void TPulseHeights::CreateHistograms(){

  // check if we already have histogramss.
  if(size() != 0){
    char tname[100];
    sprintf(tname,"PulseHeight_0_0");
    
    TH1D *tmp = (TH1D*)gDirectory->Get(tname);
    if (tmp) return;
  }

  // Otherwise make histograms
  clear();

  for(int j = 0; j < 4; j++){ // loop over modules
    for(int i = 0; i < numberChannelPerModule; i++){ // loop over 16 channels
      
      char name[100];
      char title[100];
      sprintf(name,"PulseHeight_%i_%i",j,i);
      TH1D *otmp = (TH1D*)gDirectory->Get(name);
      if (otmp) delete otmp;      
 
      sprintf(title,"Pulse Height module=%i, channel=%i",j,i);	
      
      TH1D *tmp = new TH1D(name, title, 2000, 0,500);
      tmp->SetXTitle("Pulse Height (ADC value)");
      tmp->SetYTitle("Counts");
      
      push_back(tmp);
    }
  }
}


void TPulseHeights::UpdateHistograms(TDataContainer& dataContainer){

  // Grab the data from the singleton
  std::vector<TDSPulse> pulses = dswp::instance()->GetPulses();
  for(unsigned int i = 0; i < pulses.size(); i++){
    TDSPulse pulse = pulses[i];
    GetHistogram(pulse.index)->Fill(pulse.height);
  }
}



TPulseHeightSummary::TPulseHeightSummary(){

  SetTabName("Pulses");
  SetSubTabName("Pulse Height Summary");
  CreateHistograms();
}


void TPulseHeightSummary::CreateHistograms(){
  char name[100];
  sprintf(name,"PulseHeightSummary");

  // check if we already have histogramss.
  if(size() != 0){

    TH1D *tmp = (TH1D*)gDirectory->Get(name);
    if (tmp) return;
  }

  // Otherwise make histograms
  clear();

  char title[100];
  sprintf(title,"Pulse Height Summary");

  TH2D *tmp = new TH2D(name, title, 2000, 0,500, 4*numberChannelPerModule, 0, 4*numberChannelPerModule);
  tmp->SetXTitle("Pulse Height (ADC value)");
  tmp->SetYTitle("Channel");

  push_back(tmp);
}


void TPulseHeightSummary::UpdateHistograms(TDataContainer& dataContainer){

  // Grab the data from the singleton
  std::vector<TDSPulse> pulses = dswp::instance()->GetPulses();
  for(unsigned int i = 0; i < pulses.size(); i++){
    TDSPulse pulse = pulses[i];
    GetHistogram(0)->Fill(pulse.height, pulse.index);
  }
}

TCanvasHandleBase* TPulseHeightSummary::CreateCanvas(){
  return new TSimpleHistogramCanvas(GetHistogram(0),"Pulse Height Summary", "COLZ");
}





TPulsesVsTime::TPulsesVsTime(){

  SetTabName("Pulses");
  SetSubTabName("Pulses Vs Time");
  CreateHistograms();

}


void TPulsesVsTime::CreateHistograms(){

 // check if we already have histogramss.

  TH2D *tmp = (TH2D*)gDirectory->Get("Pulses_Vs_Time");
  if(tmp){
    if(size() != 0){
      return;
    }else{
      delete tmp;
    }
  }

  tmp = new TH2D("Pulses_Vs_Time", "Number of Pulses Vs Time", 500,0,200000,64,-0.5,63.5);
  tmp->SetXTitle("Pulse Time (ns)");
  tmp->SetYTitle("Channel index");
  
  push_back(tmp);

}


void TPulsesVsTime::UpdateHistograms(TDataContainer& dataContainer){

  // Grab the data from the singleton
  std::vector<TDSPulse> pulses = dswp::instance()->GetPulses();
  for(unsigned int i = 0; i < pulses.size(); i++){
    TDSPulse pulse = pulses[i];
    GetHistogram(0)->Fill(pulse.time,pulse.index);
  }
  
}


// Create a simple canvas out of this, since we only expect one.
TCanvasHandleBase* TPulsesVsTime::CreateCanvas(){
  return new TSimpleHistogramCanvas(GetHistogram(0),"Pulses Vs Time", "COLZ");    
}



TPulseCountPDMPosition::TPulseCountPDMPosition(){

  SetTabName("Pulses");
  SetSubTabName("Pulse Count vs Position");
  CreateHistograms();

  // Map from (board*16 + channel) to (x, y)
  mb2_map[0] = std::make_pair(4,0);
  mb2_map[2] = std::make_pair(4,1);
  mb2_map[4] = std::make_pair(4,2);
  mb2_map[6] = std::make_pair(4,3);
  mb2_map[8] = std::make_pair(4,4);
  mb2_map[10] = std::make_pair(3,0);
  mb2_map[12] = std::make_pair(3,1);
  mb2_map[16] = std::make_pair(3,2);
  mb2_map[18] = std::make_pair(3,3);
  mb2_map[20] = std::make_pair(3,4);
  mb2_map[22] = std::make_pair(2,0);
  mb2_map[24] = std::make_pair(2,1);
  mb2_map[26] = std::make_pair(2,2);
  mb2_map[32] = std::make_pair(2,3);
  mb2_map[34] = std::make_pair(2,4);
  mb2_map[36] = std::make_pair(1,0);
  mb2_map[38] = std::make_pair(1,1);
  mb2_map[40] = std::make_pair(1,2);
  mb2_map[42] = std::make_pair(1,3);
  mb2_map[48] = std::make_pair(1,4);
  mb2_map[50] = std::make_pair(0,0);
  mb2_map[52] = std::make_pair(0,1);
  mb2_map[54] = std::make_pair(0,2);
  mb2_map[56] = std::make_pair(0,3);
  mb2_map[58] = std::make_pair(0,4);
}

void TPulseCountPDMPosition::CreateHistograms(){
  char name[100];
  sprintf(name,"PulseCountVsPosition");

  // check if we already have histogramss.
  if(size() != 0){

    TH1D *tmp = (TH1D*)gDirectory->Get(name);
    if (tmp) return;
  }

  // Otherwise make histograms
  clear();

  char title[100];
  sprintf(title,"Pulse Count Vs Position");

  TH2D *tmp = new TH2D(name, title, 5, 0, 5, 5, 0, 5);
  tmp->SetXTitle("PDM X");
  tmp->SetYTitle("PDM Y");

  push_back(tmp);
}

void TPulseCountPDMPosition::UpdateHistograms(TDataContainer& dataContainer){
  // Grab the data from the singleton
  std::vector<TDSPulse> pulses = dswp::instance()->GetPulses();
  for(unsigned int i = 0; i < pulses.size(); i++){
    TDSPulse pulse = pulses[i];
    if (mb2_map.find(pulse.index) != mb2_map.end()) {
      int x = mb2_map[pulse.index].first;
      int y = mb2_map[pulse.index].second;
      GetHistogram(0)->Fill(x,y);
    }
  }
}

TCanvasHandleBase* TPulseCountPDMPosition::CreateCanvas(){
  return new TSimpleHistogramCanvas(GetHistogram(0),"Pulse Count vs Position", "COLZ");
}
