#ifndef TPulseHistograms_h
#define TPulseHistograms_h

#include <string>
#include "THistogramArrayBase.h"
#include "TSimpleHistogramCanvas.hxx"

/// Class for making histograms of waveform baselines
class TPulseHeights : public THistogramArrayBase {
public:
  TPulseHeights();

  void UpdateHistograms(TDataContainer& dataContainer);  // update histograms

  void CreateHistograms(); // create histograms
  
};

class TPulseHeightSummary : public THistogramArrayBase {
public:
  TPulseHeightSummary();

  void UpdateHistograms(TDataContainer& dataContainer);  // update histograms

  void CreateHistograms(); // create histograms

  TCanvasHandleBase* CreateCanvas();
};

class TPulseCharges : public THistogramArrayBase {
public:
  TPulseCharges();

  void UpdateHistograms(TDataContainer& dataContainer);  // update histograms

  void CreateHistograms(); // create histograms
  
};

class TPulseChargeSummary : public THistogramArrayBase {
public:
  TPulseChargeSummary();

  void UpdateHistograms(TDataContainer& dataContainer);  // update histograms

  void CreateHistograms(); // create histograms

  TCanvasHandleBase* CreateCanvas();
};


class TPulsesVsTime : public THistogramArrayBase {
public:
  TPulsesVsTime();

  void UpdateHistograms(TDataContainer& dataContainer); // update histograms

  void CreateHistograms(); // create histograms
  
  // Create a simple canvas out of this, since we only expect one.
  TCanvasHandleBase* CreateCanvas();
  
};


class TPulseCountPDMPosition : public THistogramArrayBase {
public:
  TPulseCountPDMPosition();

  void UpdateHistograms(TDataContainer& dataContainer);  // update histograms

  void CreateHistograms(); // create histograms

  TCanvasHandleBase* CreateCanvas();

  std::map<int, std::pair<int, int> > mb2_map;
};


#endif


