#include "TScientificHistograms.h"

#include "TSimpleHistogramCanvas.hxx"
#include "TWaveformProcessor.hxx"
#include "TDirectory.h"
#include "TH2F.h"
#include <iostream> 
#include <cmath>

using namespace std;

const int numberChannelPerModule = 16;

/// Reset the histograms for this canvas
TTotalWaveform::TTotalWaveform(){

  SetTabName("Total Waveforms");
  SetSubTabName("Unfiltered");

  SetNanosecsPerSample(4); //ADC clock runs at 250Mhz on the v1725 = units of 4 nsecs
  fNSamples = 50000;
  CreateHistograms();
  SetNumberChannelsInGroup(numberChannelPerModule);
}


void TTotalWaveform::CreateHistograms(bool force){

  // If we are forcing to recreate histograms, delete them here.
  if(force){
    TH1D *tmp = (TH1D*)gDirectory->Get("V1725_TUW");
    if(tmp){
      if(size() != 0){
        return;
      }else{
        delete tmp;
      }
    }
    clear();
  }

  // check if we already have histograms.
  if(size() != 0){
    TH1D *tmp = (TH1D*)gDirectory->Get("V1725_TUW");
    if (tmp) return;
  }

  int WFLength = fNSamples * nanosecsPerSample; // Need a better way of detecting this...

  // Otherwise make histograms
  clear();
      
  TH1D *tmp = new TH1D("V1725_TUW", "V1725 Total Unfiltered Waveform", fNSamples, 0, WFLength);
  tmp->SetXTitle("ns");
  tmp->SetYTitle("ADC value");

  push_back(tmp);
}


void TTotalWaveform::UpdateHistograms(TDataContainer& dataContainer){
  std::vector<TDSChannel> chans = dswp::instance()->GetChannels();

  if(chans.size() != 0){
    if((int)chans[0].unfiltered_wf.size() != GetHistogram(0)->GetNbinsX()){
      fNSamples = chans[0].unfiltered_wf.size();
      CreateHistograms(true);
      std::cout << "V1725Waveforms: updating the unfiltered waveform size to " << fNSamples
          << " bins." << std::endl;
    }
  }
  else return;
  
  double wf[fNSamples];
  for(int i = 0; i < fNSamples; i++) wf[i] = 0 ;
  for(unsigned int i = 0; i < chans.size(); i++){
    for(unsigned int j = 0; j < chans[i].unfiltered_wf.size(); j++){
      wf[j] += chans[i].unfiltered_wf[j];
    }
  }
  // Fill the histogram
  for(unsigned int j = 0; j < chans[0].unfiltered_wf.size(); j++){
    GetHistogram(0)->SetBinContent((j+1),wf[j]); 
  }
}

// Create a simple canvas out of this, since we only expect one.
TCanvasHandleBase* TTotalWaveform::CreateCanvas(){
  return new TSimpleHistogramCanvas(GetHistogram(0),"Total Unfiltered Waveform", "");
}


void TTotalWaveform::Reset(){
  // Reset the histogram...
  for(int ib = 0; ib < GetHistogram(0)->GetNbinsX(); ib++) {
    GetHistogram(0)->SetBinContent(ib, 0);
  }
  GetHistogram(0)->Reset();
}


/// Reset the histograms for this canvas
TTotalFilteredWaveform::TTotalFilteredWaveform(){

  SetTabName("Total Waveforms");
  SetSubTabName("Filtered");
  SetUpdateOnlyWhenPlotted(true);

  SetNanosecsPerSample(4); //ADC clock runs at 250Mhz on the v1725 = units of 4 nsecs
  fNSamples = 50000;
  CreateHistograms();
}


void TTotalFilteredWaveform::CreateHistograms(bool force){

  // If we are forcing to recreate histograms, delete them here.
  if(force){
    TH1D *tmp = (TH1D*)gDirectory->Get("V1725_TFW");
    if(tmp){
      if(size() != 0){
        return;
      }else{
        delete tmp;
      }
    }
    clear();
  }

  // check if we already have histograms.
  if(size() != 0){
    TH1D *tmp = (TH1D*)gDirectory->Get("V1725_TFW");
    if (tmp) return;
  }

  int WFLength = fNSamples * nanosecsPerSample; // Need a better way of detecting this...

  // Otherwise make histograms
  clear();

  TH1D *tmp = new TH1D("V1725_TFW", "V1725 Total Filtered Waveform", fNSamples, 0, WFLength);
  tmp->SetXTitle("ns");
  tmp->SetYTitle("ADC value");

  push_back(tmp);
}


void TTotalFilteredWaveform::UpdateHistograms(TDataContainer& dataContainer){
  std::vector<TDSChannel> chans = dswp::instance()->GetChannels();

  if(chans.size() != 0){
    if((int)chans[0].filtered_wf.size() != GetHistogram(0)->GetNbinsX()){
      fNSamples = chans[0].filtered_wf.size();
      CreateHistograms(true);
      std::cout << "V1725Waveforms: updating the filtered waveform size to " << fNSamples
          << " bins." << std::endl;
    }
  }
  else return;

  double wf[fNSamples];
  for(int i = 0; i < fNSamples; i++) wf[i] = 0 ;
  for(unsigned int i = 0; i < chans.size(); i++){
    for(unsigned int j = 0; j < chans[i].filtered_wf.size(); j++){
      wf[j] += chans[i].filtered_wf[j];
    }
  }
  // Fill the histogram
  for(unsigned int j = 0; j < chans[0].filtered_wf.size(); j++){
    GetHistogram(0)->SetBinContent((j+1),wf[j]);
  }
}

// Create a simple canvas out of this, since we only expect one.
TCanvasHandleBase* TTotalFilteredWaveform::CreateCanvas(){
  return new TSimpleHistogramCanvas(GetHistogram(0),"Total Filtered Waveform", "");
}


void TTotalFilteredWaveform::Reset(){
  // Reset the histogram...
  for(int ib = 0; ib < GetHistogram(0)->GetNbinsX(); ib++) {
    GetHistogram(0)->SetBinContent(ib, 0);
  }
  GetHistogram(0)->Reset();
}




/// Reset the histograms for this canvas
TTotalFilteredAreaOverHeight::TTotalFilteredAreaOverHeight(){

  SetTabName("Total Waveforms");
  SetSubTabName("Area/Height");
  SetUpdateOnlyWhenPlotted(false);

  CreateHistograms();
}


void TTotalFilteredAreaOverHeight::CreateHistograms(bool force){

  // If we are forcing to recreate histograms, delete them here.
  if(force){
    TH1D *tmp = (TH1D*)gDirectory->Get("V1725_FW_AreaOverHeight");
    if(tmp){
      if(size() != 0){
        return;
      }else{
        delete tmp;
      }
    }
    clear();
  }

  // check if we already have histograms.
  if(size() != 0){
    TH1D *tmp = (TH1D*)gDirectory->Get("V1725_FW_AreaOverHeight");
    if (tmp) return;
  }

  // Otherwise make histograms
  clear();

  TH1D *tmp = new TH1D("V1725_FW_AreaOverHeight", "Total Area/Height", 1000, 0, 10000);
  tmp->SetXTitle("Total filtered area / height");

  push_back(tmp);
}


void TTotalFilteredAreaOverHeight::UpdateHistograms(TDataContainer& dataContainer){
  std::vector<TDSChannel> chans = dswp::instance()->GetChannels();

  if(chans.size() == 0) {
    return;
  }

  int nsamp = chans[0].filtered_wf.size();
  double total_wf[nsamp];
  for(int s = 0; s < nsamp; s++) total_wf[s] = 0 ;

  for(unsigned int i = 0; i < chans.size(); i++){
    for(unsigned int s = 0; s < chans[i].filtered_wf.size(); s++){
      total_wf[s] += chans[i].filtered_wf[s];
    }
  }

  double charge = 0;
  double height = 0;

  for (int s = 0; s < nsamp; s++) {
    double pos_val = -1 * total_wf[s];
    if (pos_val > height) {
      height = pos_val;
    }
    charge += pos_val;
  }
  // Fill the histogram
  if (charge > 0 && height > 0) {
    GetHistogram(0)->Fill(charge/height);
  }
}

// Create a simple canvas out of this, since we only expect one.
TCanvasHandleBase* TTotalFilteredAreaOverHeight::CreateCanvas(){
  return new TSimpleHistogramCanvas(GetHistogram(0),"Area/Height", "");
}


void TTotalFilteredAreaOverHeight::Reset(){
  // Reset the histogram...
  for(int ib = 0; ib < GetHistogram(0)->GetNbinsX(); ib++) {
    GetHistogram(0)->SetBinContent(ib, 0);
  }
  GetHistogram(0)->Reset();
}







/// TTotalTimes class: plots the times of all found pulses
/// Reset the histograms for this canvas
TTotalTimes::TTotalTimes(){

  SetSubTabName("Arrival Times of all found pulses");

  SetNanosecsPerSample(4); //ADC clock runs at 250Mhz on the v1725 = units of 4 nsecs
  fNSamples = 50000;
  CreateHistograms();
  SetNumberChannelsInGroup(numberChannelPerModule);
}


void TTotalTimes::CreateHistograms(bool force){

  // If we are forcing to recreate histograms, delete them here.
  if(force){
    TH1D *tmp = (TH1D*)gDirectory->Get("TTT");
    if(tmp){
      if(size() != 0){
        return;
      }else{
        delete tmp;
      }
    }
    clear();
  }

  // check if we already have histograms.
  if(size() != 0){
    TH1D *tmp = (TH1D*)gDirectory->Get("TTT");
    if (tmp) return;
  }

  int WFLength = fNSamples * nanosecsPerSample; // Need a better way of detecting this...

  // Otherwise make histograms
  clear();	
      
  TH1D *tmp = new TH1D("TTT", "Arrival Times of all found pulses", fNSamples, 0, WFLength);
  tmp->SetXTitle("ns");
  tmp->SetYTitle("Pulse Height");

  push_back(tmp);
}


void TTotalTimes::UpdateHistograms(TDataContainer& dataContainer){
  std::vector<TDSPulse> pulses = dswp::instance()->GetPulses();
  std::vector<TDSChannel> chans = dswp::instance()->GetChannels();

  if(chans.size() != 0){
    if((int)chans[0].filtered_wf.size() != GetHistogram(0)->GetNbinsX()){
      fNSamples = chans[0].filtered_wf.size();
      CreateHistograms(true);
    }
  }
  else return;

  Reset();
  // Fill the histogram
  for(unsigned int j = 0; j < pulses.size(); j++){
    GetHistogram(0)->AddBinContent(int(round(pulses[j].time/nanosecsPerSample)),pulses[j].height); 
  }
}

// Create a simple canvas out of this, since we only expect one.
TCanvasHandleBase* TTotalTimes::CreateCanvas(){
  return new TSimpleHistogramCanvas(GetHistogram(0),"Arrival Times of all found pulses", "COLZ");    
}


void TTotalTimes::Reset(){
  // Reset the histogram...
  for(int ib = 0; ib < GetHistogram(0)->GetNbinsX(); ib++) {
    GetHistogram(0)->SetBinContent(ib, 0);
  }
  GetHistogram(0)->Reset();
}


/// TF90VsNPE class: plots the F90 vs NPE of all events
/// Reset the histograms for this canvas
TF90VsNPE::TF90VsNPE(){

  SetSubTabName("F90 vs NPE");
  SetUpdateOnlyWhenPlotted(false);

  SetNanosecsPerSample(4); //ADC clock runs at 250Mhz on the v1725 = units of 4 nsecs
  fNSamples = 50000;
  CreateHistograms();
  SetNumberChannelsInGroup(numberChannelPerModule);
}


void TF90VsNPE::CreateHistograms(bool force){

  // If we are forcing to recreate histograms, delete them here.
  if(force){
    TH2D *tmp = (TH2D*)gDirectory->Get("F90vsNPE");
    if(tmp){
      if(size() != 0){
        return;
      }else{
        delete tmp;
      }
    }
    clear();
  }

  // check if we already have histograms.
  if(size() != 0){
    TH2D *tmp = (TH2D*)gDirectory->Get("F90vsNPE");
    if (tmp) return;
  }

  // Otherwise make histograms
  clear();	
      
  TH2D *tmp = new TH2D("F90vsNPE", "F90 Vs NPE", 500, 0, 25000,100,0,1);
  tmp->SetXTitle("NPE");
  tmp->SetYTitle("F90");

  push_back(tmp);
}


void TF90VsNPE::UpdateHistograms(TDataContainer& dataContainer){
  std::vector<TDSPulse> pulses = dswp::instance()->GetPulses();
 
  // Fill the histogram
  double fast_height = 0;
  double tot_height = 0;
  double t0 = 0;
  if(pulses.size() == 0) return;
  sort(pulses.begin(), pulses.end(), [](TDSPulse p1, TDSPulse p2) {return p1.time < p2.time;});
  t0 = pulses[0].time;
  for(unsigned int j = 0; j < pulses.size(); j++){
    if(pulses[j].time - t0 <= 90) fast_height += pulses[j].height;
    if(pulses[j].time - t0 < 30000) tot_height += pulses[j].height; // patch to ignore the REAL pulses currently registered in run635
 }
  GetHistogram(0)->Fill(tot_height,fast_height/tot_height); 
}

// Create a simple canvas out of this, since we only expect one.
TCanvasHandleBase* TF90VsNPE::CreateCanvas(){
  return new TSimpleHistogramCanvas(GetHistogram(0),"F90 Vs NPE", "COLZ");    
}


void TF90VsNPE::Reset(){
  // Reset the histogram...
  for(int ib = 0; ib < GetHistogram(0)->GetNbinsX(); ib++) {
    GetHistogram(0)->SetBinContent(ib, 0);
  }
  GetHistogram(0)->Reset();
}


/// TNPESpectrum class: plots the NPE spectrum of all events
/// Reset the histograms for this canvas
TNPESpectrum::TNPESpectrum(){

  SetSubTabName("NPE Spectrum");
  SetUpdateOnlyWhenPlotted(false);

  CreateHistograms();
  SetNumberChannelsInGroup(numberChannelPerModule);
}


void TNPESpectrum::CreateHistograms(bool force){

  // If we are forcing to recreate histograms, delete them here.
  if(force){
    TH1D *tmp = (TH1D*)gDirectory->Get("NPESpectrum");
    if(tmp){
      if(size() != 0){
        return;
      }else{
        delete tmp;
      }
    }
    clear();
  }

  // check if we already have histograms.
  if(size() != 0){
    TH1D *tmp = (TH1D*)gDirectory->Get("NPESpectrum");
    if (tmp) return;
  }
  // Otherwise make histograms
  clear();	
      
  TH1D *tmp = new TH1D("NPESpectrum", "NPE Spectrum", 500, 0, 25000);
  tmp->SetXTitle("NPE");
  tmp->SetYTitle("Events");

  push_back(tmp);
}


void TNPESpectrum::UpdateHistograms(TDataContainer& dataContainer){
  std::vector<TDSPulse> pulses = dswp::instance()->GetPulses();
 
  // Fill the histogram
  double tot_height = 0;
  if(pulses.size() == 0) return;
  for(unsigned int j = 0; j < pulses.size(); j++){
    if(pulses[j].time < 110000) tot_height += pulses[j].height; // patch to ignore the REAL pulses currently registered in run635
 }
  GetHistogram(0)->Fill(tot_height); 
}

// Create a simple canvas out of this, since we only expect one.
TCanvasHandleBase* TNPESpectrum::CreateCanvas(){
  return new TSimpleHistogramCanvas(GetHistogram(0),"NPE Spectrum", "COLZ");    
}


void TNPESpectrum::Reset(){
  // Reset the histogram...
  for(int ib = 0; ib < GetHistogram(0)->GetNbinsX(); ib++) {
    GetHistogram(0)->SetBinContent(ib, 0);
  }
  GetHistogram(0)->Reset();
}
