#include "TV1725Waveform.h"

#include "TV1725RawData.h"
#include "TDirectory.h"
#include "TWaveformProcessor.hxx"

#include <iostream> 

using namespace std;

const int numberChannelPerModule = 16;

/// Reset the histograms for this canvas
TV1725Waveform::TV1725Waveform() {

  SetTabName("V1725 Waveforms");
  SetSubTabName("Unfiltered");
  SetUpdateOnlyWhenPlotted(false); // So visible on web

  SetNanosecsPerSample(4); //ADC clock runs at 250Mhz on the v1725 = units of 4 nsecs
  fNSamples = 50000;
  CreateHistograms();
  SetNumberChannelsInGroup(numberChannelPerModule);
}

void TV1725Waveform::CreateHistograms(bool force) {

  // If we are forcing to recreate histograms, delete them here.
  if (force) {
    for (int j = 0; j < 4; j++) { // loop over modules
      for (int i = 0; i < numberChannelPerModule; i++) { // loop over 16 channels
        char tname[100];
        sprintf(tname, "V1725_UW_%i_%i", j, i);
        TH1D *tmp = (TH1D*) gDirectory->Get(tname);
        delete tmp;
      }
    }
    clear();
  }

  // check if we already have histograms.
  if (size() != 0) {
    char tname[100];
    sprintf(tname, "V1725_UW_%i_%i", 0, 0);

    TH1D *tmp = (TH1D*) gDirectory->Get(tname);
    if (tmp)
      return;
  }

  int WFLength = fNSamples * nanosecsPerSample; // Need a better way of detecting this...

  // Otherwise make histograms
  clear();

  for (int j = 0; j < 4; j++) { // loop over modules
    for (int i = 0; i < numberChannelPerModule; i++) { // loop over 16 channels

      char name[100];
      char title[100];
      sprintf(name, "V1725_UW_%i_%i", j, i);
      TH1D *otmp = (TH1D*) gDirectory->Get(name);
      if (otmp)
        delete otmp;

      sprintf(title, "V1725 Waveform for module=%i, channel=%i", j, i);

      TH1D *tmp = new TH1D(name, title, fNSamples, 0, WFLength);
      tmp->SetXTitle("ns");
      tmp->SetYTitle("ADC value");

      push_back(tmp);
    }
  }
}

void TV1725Waveform::UpdateHistograms(TDataContainer& dataContainer) {

  //    char name[100];
  //sprintf(name,"W2%02d",iBoard); 

  for (int j = 0; j < 4; j++) {  // loop over modules

    char name[100];
    sprintf(name, "W2%02i", j);
    TV1725RawData *v1725 = dataContainer.GetEventData<TV1725RawData>(name);
    if (!v1725) { // Check for ZLE data instead
      sprintf(name, "ZL%02i", j);
      v1725 = dataContainer.GetEventData<TV1725RawData>(name);
      //      cout << name << /*!v1725 << "\t\t&&" << v1725->IsZLECompressed() <<*/ endl;
    }

    if (v1725 && v1725->IsZLECompressed()) {

      for (int i = 0; i < numberChannelPerModule; i++) { // loop over channels

        // Check if this channel has any data in this event.
        int chhex = 1 << i;
        if (!(v1725->GetChannelMask() & chhex)) {
          continue;
        }

        TV1725RawChannel channelData = v1725->GetChannelData(i);
        // Use the online baseline to set the default values for all bins.
        int baseline = channelData.GetBaseline();
        if (baseline == -1)
          baseline = 0; // this indicates no baseline available.

        int index = i + j * numberChannelPerModule;
        // Reset the histogram...
        for (int ib = 0; ib < fNSamples; ib++)
          GetHistogram(index)->SetBinContent(ib + 1, baseline);

        // Loop over pulses, filling the histogram
        for (int j = 0; j < channelData.GetNZlePulses(); j++) {
          TV1725RawZlePulse pulse = channelData.GetZlePulse(j);
          for (int k = 0; k < pulse.GetNSamples(); k++) {
            int bin = pulse.GetFirstBin() + k;
            GetHistogram(index)->SetBinContent(bin, pulse.GetSample(k));
          }
        }

      }
    }

    if (v1725 && !v1725->IsZLECompressed()) {

      for (int i = 0; i < numberChannelPerModule; i++) { // loop over channels
        int index = i + j * numberChannelPerModule;
        ;
        TV1725RawChannel channelData = v1725->GetChannelData(i);
        if (i == 0 and j == 0) {

          if (channelData.GetNSamples() != GetHistogram(index)->GetNbinsX()) {

            fNSamples = channelData.GetNSamples();
            CreateHistograms(true);
            std::cout << "V1725Waveforms: updating the waveform size to " << fNSamples << " bins." << std::endl;

          }
        }

        if (0) {
          static unsigned int lasttime = 0;
          uint32_t ett = v1725->GetExtendedTimeTag();
          if (ett != lasttime) {
            printf("time time: %i %i %u\n", ett, lasttime, (unsigned) time(NULL));
          }
          lasttime = ett;
        }

        // Reset the histogram...
        //        for(int ib = 0; ib < 2500; ib++)
        double baseline = 0;
        if (channelData.GetNSamples())
          baseline = channelData.GetADCSample(channelData.GetNSamples() - 1);
        for (int ib = 0; ib < fNSamples; ib++)
          GetHistogram(index)->SetBinContent(ib + 1, baseline);

        for (int j = 0; j < channelData.GetNSamples(); j++) {
          double adc = channelData.GetADCSample(j);
          GetHistogram(index)->SetBinContent(j + 1, adc);

        }
      }
    }
  }
}

void TV1725Waveform::Reset() {

  for (int j = 0; j < 4; j++) {        // loop over modules

    for (int i = 0; i < numberChannelPerModule; i++) { // loop over channels
      int index = i + j * numberChannelPerModule;
      ;;

      // Reset the histogram...
      for (int ib = 0; ib < GetHistogram(index)->GetNbinsX(); ib++) {
        GetHistogram(index)->SetBinContent(ib, 0);
      }

      GetHistogram(index)->Reset();

    }
  }
}

TV1725FilteredWaveform::TV1725FilteredWaveform() {

  SetTabName("V1725 Waveforms");
  SetSubTabName("Filtered");

  SetNanosecsPerSample(4); //ADC clock runs at 250Mhz on the v1725 = units of 4 nsecs
  fNSamples = 50000;
  CreateHistograms();
  SetNumberChannelsInGroup(numberChannelPerModule);
}

void TV1725FilteredWaveform::CreateHistograms(bool force) {

  // If we are forcing to recreate histograms, delete them here.
  if (force) {
    for (int j = 0; j < 4; j++) { // loop over modules
      for (int i = 0; i < numberChannelPerModule; i++) { // loop over 16 channels
        char tname[100];
        sprintf(tname, "V1725_FW_%i_%i", j, i);
        TH1D *tmp = (TH1D*) gDirectory->Get(tname);
        delete tmp;
      }
    }
    clear();
  }

  // check if we already have histograms.
  if (size() != 0) {
    char tname[100];
    sprintf(tname, "V1725_FW_%i_%i", 0, 0);

    TH1D *tmp = (TH1D*) gDirectory->Get(tname);
    if (tmp)
      return;
  }

  int WFLength = fNSamples * nanosecsPerSample; // Need a better way of detecting this...

  // Otherwise make histograms
  clear();

  for (int j = 0; j < 4; j++) { // loop over modules
    for (int i = 0; i < numberChannelPerModule; i++) { // loop over 16 channels

      char name[100];
      char title[100];
      sprintf(name, "V1725_FW_%i_%i", j, i);
      TH1D *otmp = (TH1D*) gDirectory->Get(name);
      if (otmp)
        delete otmp;

      sprintf(title, "V1725 Filtered Waveform for module=%i, channel=%i", j, i);

      TH1D *tmp = new TH1D(name, title, fNSamples, 0, WFLength);
      tmp->SetXTitle("ns");
      tmp->SetYTitle("ADC value");

      push_back(tmp);
    }
  }
}

void TV1725FilteredWaveform::UpdateHistograms(TDataContainer& dataContainer) {
  std::vector<TDSChannel>& channels = dswp::instance()->GetChannels();

  for (unsigned int i = 0; i < channels.size(); i++) {
    for (unsigned int s = 0; s < channels[i].filtered_wf.size(); s++) {
      GetHistogram(i)->SetBinContent(s + 1, channels[i].filtered_wf[s]);
    }
  }

}

void TV1725FilteredWaveform::Reset() {

  for (int j = 0; j < 4; j++) { // loop over modules

    for (int i = 0; i < numberChannelPerModule; i++) { // loop over channels
      int index = i + j * numberChannelPerModule;
      ;;

      // Reset the histogram...
      for (int ib = 0; ib < GetHistogram(index)->GetNbinsX(); ib++) {
        GetHistogram(index)->SetBinContent(ib, 0);
      }

      GetHistogram(index)->Reset();

    }
  }
}
