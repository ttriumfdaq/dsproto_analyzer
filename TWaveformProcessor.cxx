#include "TWaveformProcessor.hxx"
#include "TV1725RawData.h"
#include "eulero.hxx"
#include "TPulseInjector.hxx"
#include "math.h"
#include <algorithm>
#include <iostream>

using namespace std;

// Allocating and initializing GlobalClass's
// static data member.  
TWaveformProcessor* TWaveformProcessor::s_instance = 0;

TWaveformProcessor::TWaveformProcessor() {
  r.SetSeed(0);
}
;

TWaveformProcessor* TWaveformProcessor::instance() {
  if (!s_instance)
    s_instance = new TWaveformProcessor();
  return s_instance;
}

void TWaveformProcessor::Reset() {
  channels.clear();
  pulses.clear();
  r.SetSeed(0);
}

int TWaveformProcessor::ProcessWaveforms(TDataContainer& dataContainer) {

  // reset things
  channels.clear();
  pulses.clear();

  //  std::cout << "Processing event " << dataContainer.GetMidasEvent().GetSerialNumber() << std::endl;

  // Loop over the four V1725s
  for (int j = 0; j < 4; j++) { // loop over modules

    char name[100];
    sprintf(name, "W2%02i", j);
    TV1725RawData *v1725 = dataContainer.GetEventData<TV1725RawData>(name);
    if (!v1725) { // Check for ZLE data instead
      sprintf(name, "ZL%02i", j);
      v1725 = dataContainer.GetEventData<TV1725RawData>(name);
    }

    if (!v1725) {
      continue;
    }
    if (v1725 && v1725->IsZLECompressed())
      continue; // Not implemented

    if (1/*v1725 && !v1725->IsZLECompressed()*/) {
      for (int i = 0; i < 16; i++) { // loop over channels
        TV1725RawChannel channelData = v1725->GetChannelData(i);
        if (channelData.IsEmpty())
          continue;
        // Calculate baseline using first 200 samples
        const int nbase_samples = 200;
        double baseline = 0;
        double baseline_rms = 0;
        for (int ib = 0; ib < nbase_samples; ib++) {
          baseline += channelData.GetADCSample(ib);
          baseline_rms += channelData.GetADCSample(ib) * channelData.GetADCSample(ib);
        }
        baseline /= (float) nbase_samples;
        baseline_rms /= (float) nbase_samples;
        baseline_rms -= baseline * baseline;
        baseline_rms = sqrt(baseline_rms);
        //        cout << "Baseline:\t" << baseline << "\t\tBaseline RMS:\t" << baseline_rms <<"\t\tChan\t"<<i<<"\t\tBoard\t"<<j<< endl;

        double roi_charge = 0;
        for (int ib = 2300; ib < 3125; ib++) {
          double sample = channelData.GetADCSample(ib);
          roi_charge += baseline - sample;
        }

        // Store results
        TDSChannel tmp(j, i);
        tmp.baseline = baseline;
        tmp.baseline_rms = baseline_rms;
        tmp.roicharge = roi_charge;
        // Copy unfiltered waveform in the TDSChannel structure
        tmp.unfiltered_wf.clear();
        for (int ib = 0; ib < channelData.GetNSamples(); ib++) {
          tmp.unfiltered_wf.push_back(channelData.GetADCSample(ib) - baseline);
        }
        channels.push_back(tmp);
      }
    }
  }
  return 1;
}

void TWaveformProcessor::SetEuler() {
  eu.set_tau(80);
  eu.set_sigma_fp(2.);
  eu.set_Afp_Aexp_ratio(1);
  eu.gen_MA_gaus_ker();
  eu.calc_fast_exp_ratio();
  eu.gen_ARMA_ker();
  return;
}

void TWaveformProcessor::InjectPulses(TPInjector tpi, int pulse_type) {
  if (pulse_type == 0) {
    for (unsigned int ch = 0; ch < channels.size(); ch++)
      tpi.InjectPoissonNPE(channels[ch].unfiltered_wf);
  } else if (pulse_type == 1) {
    int NPE = 0;
    float LY = 1.; // NPE/keV
    while (NPE == 0) {
      float E = r.Uniform(1, 500); // keV
      NPE = r.Poisson(E * LY);
    }
    int NPEf = r.Binomial(NPE, 0.3);
    int sample0 = 0;
    double tauf = 7; // ns
    double taus = 1600; // ns
    int count_fast = 0;
    int ch = -1;
    int wf_size = 0;
    if (channels.size() > 0) {
      wf_size = channels[0].unfiltered_wf.size();
    } else {
      return;
    }
    for (int i = 0; i < NPE; i++) {
      // extract time according to exponential distributions
      if (count_fast < NPEf) {
        sample0 = int(round(tpi.get_pretrigger() * wf_size) + round(r.Gaus(0, tpi.get_jitter())) + round(r.Exp(tauf) / 4));
      } else {
        sample0 = int(round(tpi.get_pretrigger() * wf_size) + round(r.Gaus(0, tpi.get_jitter())) + round(r.Exp(taus) / 4));
      }
      count_fast++;
      ch = r.Integer(channels.size()); // extract a random channel on which inject the pulse
      tpi.InjectPulse(channels[ch].unfiltered_wf, sample0, 1);
    }
  } else
    return;
}

int TWaveformProcessor::FilterWaveforms() {

  for (unsigned int ch = 0; ch < channels.size(); ch++) {
    // moving average filtering for the fast peak   
    int nsamples = channels[ch].unfiltered_wf.size();
    int kgsize = eu.get_MA_gaus_ker().size();
    if (nsamples == 0 || kgsize == 0)
      continue;
    std::vector<double> gker = eu.get_MA_gaus_ker();
    std::vector<double> MA(nsamples);
    int i, j;
    double tmp;
    for (i = 0; i < nsamples; i++) {
      tmp = 0.;
      if (i - std::ceil(kgsize / 2.) + 1 < 0 || i + kgsize / 2 >= nsamples) {
        MA[i] = 0;
      } else {
        for (j = 0; j < kgsize; j++) {
          tmp += channels[ch].unfiltered_wf[i + kgsize / 2 - j] * gker[j];
        }
      }
      MA[i] = tmp;
    }
    // Auto-Recursive filtering for the exponential tail
    std::vector<double> AR;
    AR.clear();
    double a = 1 - 1. / eu.get_tau();
    AR.push_back(channels[ch].unfiltered_wf[nsamples - 1]);
    for (i = 1; i < nsamples; i++) {
      AR.push_back(channels[ch].unfiltered_wf[nsamples - 1 - i] + a * AR[i - 1]);
    }
    std::reverse(AR.begin(), AR.end());
    for (i = 0; i < nsamples; i++) {
      AR[i] /= eu.get_tau();
    }
    // ARMA put together: write vector in TDSChannel structure
    channels[ch].filtered_wf.clear();
    double scale_factor = eu.get_fast_exp_charge_ratio();
    for (i = 0; i < (int)channels[ch].unfiltered_wf.size(); i++) {
      channels[ch].filtered_wf.push_back((1. - scale_factor) * AR[i] + scale_factor * MA[i]);
    }
  }
  return 1;
}

int TWaveformProcessor::FindPulses() {
  // Loop for pulses.  Define a pulse as being pulse_threshold counts below the baseline.
  static double pulse_threshold = 68.0;
  bool in_pulse = false;
  double pulse_height = 9999999.0;
  double pulse_time = 0.0;
  double pulse_charge = 0.0;
  double sample = 0;
  int i = -1, j = -1;
  for (unsigned int c = 0; c < channels.size(); c++) {
    i = channels[c].module;
    j = channels[c].channel;
    for (unsigned int iw = 0; iw < channels[c].filtered_wf.size(); iw++) {
      // Make the sample value positive so over/under and integral makes more sense.
      sample = -1 * channels[c].filtered_wf[iw];
      // if not already in a pulse, check if we went above threshold
      if (!in_pulse) {
        if (sample > pulse_threshold) { // found pulse
          in_pulse = true;
          pulse_charge = 0.0;
          pulse_height = sample;
          pulse_time = iw * 4; // 4 is the number of nanoseconds per sample
        }
      } else {
        pulse_charge += sample;
        // otherwise, check if we reach pulse min...
        if (sample > pulse_height) {
          pulse_height = sample;
          pulse_time = iw * 4; // 4 is the number of nanoseconds per sample
        }
        // ... and look for pulse going back below threshold.
        if (sample <= pulse_threshold) { // pulse is finished)
          if (0) std::cout << "Found pulse: " << pulse_height << " " << pulse_time << std::endl;

          // Store results
          TDSPulse pulse(i, j);
          pulse.height = pulse_height;
          pulse.time = pulse_time;
          pulse.charge = pulse_charge;
          pulses.push_back(pulse);
          in_pulse = false;
        }
      }
    }
  }
  return 1;
}
