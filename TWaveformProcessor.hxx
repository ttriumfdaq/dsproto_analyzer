#ifndef TWaveformProcessor_h
#define TWaveformProcessor_h

#include <vector>
#include "TDataContainer.hxx"
#include "TPulseInjector.hxx"
#include "eulero.hxx"
#include <TRandom3.h>

/// Class to store a pulse
class TDSPulse{

public:
  TDSPulse(int imodule = -1, int ichannel = -1){
    time = 0;
    height = 0;
    charge=0;
    module = imodule;
    channel = ichannel;
    index = module*16 + channel;
  }
    

  int module;
  int channel;
  int index;
  double charge;
  double time;
  double height;

};


/// Class to store info about a waveform (ie baseline)
class TDSChannel{

public:
  TDSChannel(int imodule = -1, int ichannel = -1){
    baseline = 0;
    baseline_mean = 0;
    baseline_rms = 0;
    baseline_rms_mean = 0;
    unfiltered_wf.clear();
    filtered_wf.clear();
    roicharge = 0;
    module = imodule;
    channel = ichannel;
    index = module*16 + channel;
  }
  double baseline;
  double baseline_mean;
  double baseline_rms;
  double baseline_rms_mean;
  vector<double> unfiltered_wf;
  vector<double> filtered_wf;
  double roicharge;
  int module;
  int channel;
  int index;
};

/// Make singleton to analyze the V1725 waveforms and store results
class TWaveformProcessor
{
public:
  
  static TWaveformProcessor *instance();

  // Delete hits from last event.
  void Reset();

  std::vector<TDSPulse>& GetPulses(){ return pulses;}
  std::vector<TDSChannel>& GetChannels(){ return channels;}
  eulero& GetEuler() { return eu;}
 
  int ProcessWaveforms(TDataContainer& dataContainer);
  void SetEuler(); 
  void InjectPulses(TPInjector tpi, int pulse_type);
  int FilterWaveforms();
  int FindPulses();

private:
  
  // pointer to global object
  static TWaveformProcessor  *s_instance;

  // hidden private constructor
  TWaveformProcessor();

  std::vector<TDSPulse> pulses;
  std::vector<TDSChannel> channels;
  eulero eu;  
  TRandom3 r; 
};

// Setup a short name for class dswp = Darkside Waveform Processor
typedef TWaveformProcessor dswp;



#endif
