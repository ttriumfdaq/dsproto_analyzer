"""
Tools for calculating the baseline of V1725 data.
"""

import midas
import midas.event
import proto_data_format
import sys
import root_setup
import ROOT

def calculate_from_live_events(client, buffer_name, num_events, board_list, num_samples, print_status=True):
    """
    Calculate baseline by reading events from a midas buffer.
    
    Args:
        * client (`midas.client.MidasClient`)
        * buffer_name (str) - Probably "SYSTEM".
        * num_events (int) - Number of events to look at to compute baselines.
        * board_list (int) - List of board IDs to interact with (assumes all controlled by Equipment V1725_Data00)
        * num_samples (int) - Number of samples at start of waveform to use when calculating baseline (1500=6us)
        * print_status (bool) - Whether to print progress/baselines to screen.
    
    Returns:
        dict of {int: {int: (int, int)}} for {board: {channel: (mean, rms)}}
    """
    if client.odb_get("/Runinfo/State") != midas.STATE_RUNNING:
        raise RuntimeError("A run must be in progress, so we can analyze the existing waveforms")

    buffer_handle = client.open_event_buffer(buffer_name)
    request_id = client.register_event_request(buffer_handle, 1, sampling_type=midas.GET_RECENT)
    
    prev_parsed_events = -1
    parsed_events = 0
    histos = {}
    baselines = {}
    
    while parsed_events < num_events:
        if parsed_events != prev_parsed_events:
            if print_status:
                sys.stdout.write("\rWaiting for event %s/%s" % (parsed_events + 1, num_events))
            prev_parsed_events = parsed_events
        
        client.communicate(1)
        
        midas_event = client.receive_event(buffer_handle)
        proto_event = proto_data_format.parse(midas_event)
    
        if not proto_event:
            continue
        
        parsed_events += 1
        
        if print_status:
            sys.stdout.write("\rProcessing  event %s/%s" % (parsed_events, num_events))
        
        for board_idx, board_data in proto_event.v1725s.items():
            if board_idx not in board_list:
                continue

            if board_idx not in histos:
                histos[board_idx] = {}
                
            for chan, wf in enumerate(board_data.waveforms):
                if len(wf) == 0:
                    continue
                
                if chan not in histos[board_idx]:
                    histos[board_idx][chan] = ROOT.TH1D("base_%s_%s" % (board_idx, chan), "", 16384, 0, 16384)
                
                for s in wf[:num_samples]:
                    histos[board_idx][chan].Fill(s)
    
    if print_status:
        print("\nComputing baselines")
    
    for board_idx, chans in histos.items():
        baselines[board_idx] = {}
            
        for chan, histo in chans.items():
            fit = histo.Fit("gaus", "SQ")
            mean = fit.Get().Parameter(1)
            rms = fit.Get().Parameter(2)
            maxi = histo.GetBinLowEdge(histo.GetMaximumBin())
            baselines[board_idx][chan] = (mean, rms, maxi)
            
            if print_status:
                print("Baseline of board %s channel %s is %.2f (RMS %.2f; Max bin %i)" % (board_idx, chan, mean, rms, maxi))
                
    client.deregister_event_request(buffer_handle, request_id)
    
    return baselines    