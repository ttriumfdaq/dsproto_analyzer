#define _USE_MATH_DEFINES

#include <cmath>
#include <iostream>
#include <fstream>
#include <iterator>
#include <algorithm>
#include "eulero.hxx"

eulero::eulero(){
  MA_gaus_ker.clear();
  ARMA_ker.clear();
  tau = 125;
  sigma_fp = 1.;
  Afp_Aexp_ratio = 1.;
  fast_exp_ratio = 1./125*(std::sqrt(2*M_PI));
}

eulero::eulero( eulero &eu0){
  tau = eu0.get_tau();
  sigma_fp = eu0.get_sigma_fp();
  Afp_Aexp_ratio = eu0.get_Afp_Aexp_ratio();
  fast_exp_ratio = eu0.get_fast_exp_charge_ratio();
  MA_gaus_ker.clear();
  MA_gaus_ker.assign(eu0.get_MA_gaus_ker().begin(), eu0.get_MA_gaus_ker().end());
  ARMA_ker.clear();
  ARMA_ker.assign(eu0.get_ARMA_ker().begin(), eu0.get_ARMA_ker().end()); 
 }

void eulero::calc_fast_exp_ratio(){
  fast_exp_ratio = Afp_Aexp_ratio/tau*(std::sqrt(2*M_PI)*sigma_fp);
}

void eulero::gen_MA_gaus_ker(){
  MA_gaus_ker.clear();
  float s = float(sigma_fp);
  for(int i = 0; i < 8*int(s); i++) MA_gaus_ker.push_back(exp( -0.5*pow((i-4*s)/s,2) ) / (s*sqrt(2*M_PI)) );
}

void eulero::gen_ARMA_ker(){
  ARMA_ker.clear();
  double par[3] = {tau, Afp_Aexp_ratio, sigma_fp};
  int nsamples_pulse = static_cast<int>(4*par[0]+4*par[2]);
  vector<double> exp_ker;
  for(int i = 0; i < nsamples_pulse; i++){
    if(i < 4*par[2]) exp_ker.push_back(0);
    else exp_ker.push_back(exp(-(i-4*par[2])/par[0])/par[0]);
  }
  double max_exp = 1./par[0];
  vector<double> gaus_ker;
  if( MA_gaus_ker.size() != 0) gaus_ker.assign(MA_gaus_ker.begin(), MA_gaus_ker.end());
  else 
    for(int i = 0; i < 8*par[2]; i++) gaus_ker.push_back(exp( -0.5*pow((i-4*par[2])/par[2],2) ) / (par[2]*sqrt(2*M_PI)) );
  int i, j;
  double tmp;
  double integral = 0;
  for( i=0; i<nsamples_pulse; i++){
    tmp = 0;
    for( j=0; j<(int)gaus_ker.size(); j++ )
      if( j<=i && (i+j)<nsamples_pulse ) tmp += exp_ker[i+j]*gaus_ker[j];
    tmp += max_exp*par[1]*exp( -0.5*pow((i-4*par[2])/par[2],2) );
    integral += tmp;
    ARMA_ker.push_back(tmp);
  } 
  double max = *max_element(ARMA_ker.begin(),ARMA_ker.end()); 
  for( i=0; i<nsamples_pulse; i++) ARMA_ker[i] /= max;
}
