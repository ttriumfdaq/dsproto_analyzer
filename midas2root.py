import ROOT
import midas.file_reader
import argparse
import re
import proto_data_format
import sys
import numpy

"""
Script to convert data read with dsproto_daq (V1725 DAQ) to TH1Fs,
in same format as used for Genova data.
"""

def process(input, output, min_evt_id, max_evt_id):
    root_file = ROOT.TFile.Open(output, "RECREATE")
    midas_file = midas.file_reader.MidasFile(input)
    run_num = midas_file.get_bor_odb_dump().data["Runinfo"]["Run number"]

    num_chans_per_board = 16
    ns_per_bin = 4

    for ev in midas_file:
        evt_id = ev.header.serial_number

        if min_evt_id is not None and evt_id < min_evt_id:
            sys.stdout.write("Skipping event %d\r" % evt_id)
            continue

        if max_evt_id is not None and evt_id > max_evt_id:
            break

        sys.stdout.write("Processing event %d\r" % evt_id)

        parsed = proto_data_format.parse(ev)

        if parsed is None:
            continue

        for board_idx, board_data in parsed.v1725s.items():
            for chan_idx, chan_data in enumerate(board_data.waveforms):
                if len(chan_data) == 0:
                    continue

                global_chan_idx = board_idx * num_chans_per_board + chan_idx

                histo_name = "hwf_r%de%dc%d_pmtge_hdo_reader" % (run_num, evt_id, global_chan_idx)
                histo = ROOT.TH1F(histo_name, "Waveform from module pmtge_hdo_reader", len(chan_data), 0, len(chan_data))
                histo.GetXaxis().SetTitle("Time (ns)")
                histo.GetXaxis().SetLimits(0, len(chan_data) * ns_per_bin)
                histo.GetYaxis().SetTitle("Amplitude")

                root_data = numpy.zeros(len(chan_data) + 2, dtype="float")
                root_data[1:-1] = chan_data
                histo.SetContent(root_data)

                histo.Write()


    sys.stdout.write("\n")
    root_file.Close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", required=True)
    parser.add_argument("--output")
    parser.add_argument("--min-evt-id", type=int)
    parser.add_argument("--max-evt-id", type=int)
    args = parser.parse_args()

    if args.output is None:
        out = re.sub("\.mid.*", ".root", args.input)
    else:
        out = args.output
    
    process(args.input, out, args.min_evt_id, args.max_evt_id)
