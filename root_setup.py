"""
ROOT/python versioning is painful. The default python/ROOT on
ds-proto-daq aren't compatible with our python3 tools. This module
will set the correct paths so that the right libraries are loaded.

If you try to import the wrong version of ROOT, then try to import the
correct one, you get a segfault. You must set up the correct paths
before calling import ROOT!

Example usage:

```
import root_setup
import ROOT
```
"""

import sys
import os

if os.path.exists("/home/dsproto/packages/root6_py3"):
    # Special version of ROOT on ds-proto that works with python3
    sys.path.insert(0, "/home/dsproto/packages/root6_py3/lib")
    os.environ["ROOTSYS"] = "/home/dsproto/packages/root6_py3"
    os.environ["LD_LIBRARY_PATH"] = "/home/dsproto/packages/root6_py3/lib:%s" % os.environ["LD_LIBRARY_PATH"]
    os.environ["DYLD_LIBRARY_PATH"] = "/home/dsproto/packages/root6_py3/lib:%s" % os.environ["DYLD_LIBRARY_PATH"]
    os.environ["LIBPATH"] = "/home/dsproto/packages/root6_py3/lib:%s" % os.environ["LIBPATH"]
    os.environ["SHLIB_PATH"] = "/home/dsproto/packages/root6_py3/lib:%s" % os.environ["SHLIB_PATH"]
