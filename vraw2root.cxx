// Program for online processing and converting MIDAS format to ROOT format.
// Adapted from midas2root.cxx 
// 
// Contact: S. Stracka (simone.strackaATcern.ch)
//


#include <stdio.h>
#include <iostream>
#include <time.h>
#include <vector>

#include "TRootanaEventLoop.hxx"
#include "TFile.h"
#include "TTree.h"
#include "TStopwatch.h"
#include "TMath.h"

#include "TAnaManager.hxx"

#include "TChronoData.h"
#include "TV1725RawData.h"

#define MAXSAMPLES 640000 // size of memory in samples per channel
#define ADCCHANNELS 16    // max number of channels per ADC
#define MAXCHANNELS 32    // total number of channels 
#define DNSAMPLE 1        // downsampling factor: 1 (=no downsampling) or 2

#define NSAMPLES 50000    // number of samples in each waveform

#define NFWD  50000       // samples after trigger  (60000 = 240 us)
#define NBWD  0           // samples before trigger (2500 = 10 us)
#define NLENGTH 128       // length of running average window

class CircBuf {

  CircBuf();

  const int size; // depth of the buffer
  const int dim;  // dimension of the buffer
  int *data;
  double *sum;
  int front;
    
public:
  CircBuf(int, int);
  ~CircBuf();

  bool add(const UShort_t*);
  double* rolling_sum();
  
};

CircBuf::CircBuf(int sz, int dm): size(sz), dim(dm){
  if (sz==0) throw std::invalid_argument("size and dimension cannot be zero");

  data = new int[size*dim];
  sum = new double[dim];  
  for (int j=0; j<dim; j++){
    for (int i=0; i<size; i++){      
      data[i*dim+j]=0;
      sum[j]=0;
    }
  }

  front = 0;
  
}

CircBuf::~CircBuf() {
  delete data;
}


bool CircBuf::add(const UShort_t *t) {

  
  int ipos = front*dim;
  
  for (int i=0; i<dim; i++, ipos++){
    /*** ROLLING SUM ***/
    sum[i] -= data[ipos];
    sum[i] += t[i];       // update rolling sum
    data[ipos] = t[i];    // update buffer
  }
    
  front++;
  front %= size;
  return true;  

}


double* CircBuf::rolling_sum() {
  return sum;
}



class Analyzer: public TRootanaEventLoop {

public:

  // An analysis manager.  Define and fill histograms in 
  // analysis manager.
  TAnaManager *anaManager;

  // The tree to fill.
  TTree *fTree;
  TStopwatch* sw;
  TStopwatch* swr;

  int nevts = 0;
  int timestamp;
  int serialnumber;
  int nsamples = NSAMPLES;

  int ntriggers;
  int icandidate;
  int startidx;

  int noV1725 = 0;
  int nwritten = 0;
  int ntot = 0;
  
  int idADC[2]={0,2};
  UShort_t adc_value[MAXCHANNELS][MAXSAMPLES];
  UShort_t adc_vec[MAXCHANNELS];

  std::vector<int> triggers;

  Analyzer() {

  };

  virtual ~Analyzer() {};

  void Initialize(){


  }
  
  
  void BeginRun(int transition,int run,int time){
    
    // Create a TTree
    fTree = new TTree("midas_data","MIDAS data");
    sw = new TStopwatch();
    swr = new TStopwatch();


    char name[100];
    char bname[100];
    char lname[100];
    
    fTree->Branch("timestamp",&timestamp,"timestamp/I");
    fTree->Branch("serialnumber",&serialnumber,"serialnumber/I");
    fTree->Branch("nsamples",&nsamples,"nsamples/I");
    fTree->Branch("ntriggers",&ntriggers,"ntriggers/I");
    fTree->Branch("icandidate",&icandidate,"icandidate/I");
    fTree->Branch("startidx",&startidx,"startidx/I");

    for (int i=0; i< MAXCHANNELS; i++){
    
      int iadc = i/ADCCHANNELS;
      int jch  = i%ADCCHANNELS;
      
      sprintf(name,"W2%02i_Ch%02i",idADC[iadc],jch);
      
      sprintf(bname,"adc_%s",name);
      sprintf(lname,"%s[nsamples]/s",bname);
      fTree->Branch(bname,adc_value[i],lname);

    }

  }


  void EndRun(int transition,int run,int time){

    printf("nevts: %d ; RealTime (Fill): %lg (Rolling): %lg\n",nevts , sw->RealTime(),swr->RealTime());
    printf("MIDAS events processed: %d ; MIDAS events written: %d; MIDAS events w/o V1725: %d\n", ntot, nwritten, noV1725);
  }

    
  // Main work here; create ttree events for every sequenced event 
  bool ProcessMidasEvent(TDataContainer& dataContainer){

    triggers.clear();
    ntot++;

    serialnumber = dataContainer.GetMidasEvent().GetSerialNumber();
    if(serialnumber%10 == 0) { 
      printf(".");
      nevts +=10;
    }
    timestamp = dataContainer.GetMidasEvent().GetTimeStamp();

    //TGenericData *vESID = dataContainer.GetEventData<TGenericData>("ESID");

    //TChronoData *vChrono = dataContainer.GetEventData<TChronoData>("ZMQ0");

    char name[100];

    TV1725RawChannel* chData[MAXCHANNELS];

    nsamples = NSAMPLES;
    int chNumber=0;
    int nChannels=0;
    int nADC = TMath::CeilNint(double(MAXCHANNELS)/ADCCHANNELS);
    for (int i=0; i<nADC; i++){ //iADC

      sprintf(name,"W2%02i",idADC[i]);
      TV1725RawData *v1725 = dataContainer.GetEventData<TV1725RawData>(name);
      
      if(!v1725){
	noV1725++;
	nsamples=0;
	break;	
	//return false; 
	// continue; // Maybe ZLE applied, and bank name is different
      }

      int nch = v1725->GetNChannels(); // check
      nChannels += nch;
      if (nChannels > MAXCHANNELS){
	printf("Check MAXCHANNELS!\n");
	return false;
      }

      for(int j=0; j < nch; j++, chNumber++){ // jCh
        chData[chNumber] = new TV1725RawChannel(v1725->GetChannelData(j));
	// chNumber = chData[i*MAXCHANNELS+j]->GetChannelNumber();

	if(NSAMPLES != chData[j]->GetNSamples()){	  
	  printf("Check NSAMPLES! Expecting %d, found %d\n", NSAMPLES, chData[j]->GetNSamples());
	  return false;
	}
	
      }

    }


    swr->Start(kFALSE);

    CircBuf cc(NLENGTH,nChannels);
    UShort_t baseline[nChannels];
    UShort_t charge[nChannels];
  
    int k;
    for(k = 0; k < nsamples; k+=DNSAMPLE){ 
    //    for(int k = 0; k < nsamples - NFWD + 1; k+=DNSAMPLE){ 

      // calculate rolling average
      if (k==NLENGTH) {
	for(int ic = 0; ic < nChannels; ic++){
	  baseline[ic]= (UShort_t)(cc.rolling_sum()[ic]);
	}	
      }

      for(int j = 0; j < nChannels; j++){ // jCh
	if(chData[j]->IsEmpty()) {
	  adc_vec[j] = 0;	   
	  continue;
	}
	else {
	  adc_vec[j] = (UShort_t)(chData[j]->GetADCSample(k));
	}
      }
      cc.add(adc_vec);


      // skip samples used for baseline
      // start from NLENGTH*2 to avoid correlation with baseline
      if (k>=NLENGTH*2) {

	for(int ic = 0; ic < nChannels; ic++){
	  charge[ic] = baseline[ic]-(UShort_t)(cc.rolling_sum()[ic]);
	}	

	// // take trigger decision
	// triggers.push_back(k);

      }

    }

  
    // bypass trigger above
    if(1){
      triggers.clear();
      triggers.push_back(0);
    }

    swr->Stop();

    // flush 
    for(int k = 0, is=0; k < nsamples; k+=DNSAMPLE, is++){ // init vectors
      for(int j = 0; j < nChannels; j++){ // jCh
	adc_value[j][is] = 0;	   
      }
    }

    // possibly arrange chunks
    // not dealing with overlaps: they will result in duplication, so must be kept to a minimum. 
    ntriggers = triggers.size();
    for (icandidate=0; icandidate<ntriggers; icandidate++){
     
      int istart = TMath::Max(triggers.at(icandidate)-NBWD,0); // <-- cannot be negative
      int istop =  TMath::Min(triggers.at(icandidate)+NFWD,nsamples); // <-- cannot exceed nsamples

      startidx=istart;
      int is=0;
      for(int k = istart; k < istop; k+=DNSAMPLE, is++){ // init vectors

	for(int j = 0; j < nChannels; j++){ // jCh
	  if(chData[j]->IsEmpty()) {
	    adc_value[j][is] = 0;	   
	  }
	  else {
	    adc_value[j][is] = (UShort_t)(chData[j]->GetADCSample(k));
	  }	    	  
	}
      }
      nsamples=is; // redefine nsamples; the above check in istart and istop 
                   // assignment implies is <= nsamples

      sw->Start(kFALSE);      
      fTree->Fill();
      sw->Stop();
      
      nwritten++;

    }
    

    // clean up
    for(int j=0; j < nChannels; j++)
      delete chData[j];


    return true;

  };
  
  // Complicated method to set correct filename when dealing with subruns.
  std::string SetFullOutputFileName(int run, std::string midasFilename)
  {
    char buff[128]; 
    Int_t in_num = 0, part = 0;
    Int_t num[2] = { 0, 0 }; // run and subrun values
    // get run/subrun numbers from file name
    for (int i=0; ; ++i) {
      char ch = midasFilename[i];
        if (!ch) break;
        if (ch == '/') {
          // skip numbers in the directory name
          num[0] = num[1] = in_num = part = 0;
        } else if (ch >= '0' && ch <= '9' && part < 2) {
          num[part] = num[part] * 10 + (ch - '0');
          in_num = 1;
        } else if (in_num) {
          in_num = 0;
          ++part;
        }
    }
    if (part == 2) {
      if (run != num[0]) {
        std::cerr << "File name run number (" << num[0]
                  << ") disagrees with MIDAS run (" << run << ")" << std::endl;
        exit(1);
      }
      sprintf(buff,"output_%.6d_%.4d.root", run, num[1]);
      printf("Using filename %s\n",buff);
    } else {
      sprintf(buff,"output_%.6d.root", run);
    }
    return std::string(buff);
  };





}; 


int main(int argc, char *argv[])
{

  Analyzer::CreateSingleton<Analyzer>();
  return Analyzer::Get().ExecuteLoop(argc, argv);

}

