#include "TEvaluationData.hxx"

#include <iomanip>
#include <iostream>
#include <arpa/inet.h>

TADC::TADC(int bklen, int bktype, const char *name, void *pdata) :
    TGenericData(bklen, bktype, name, pdata) {

  // Data format is simply a list of WORDs
  const uint16_t* p = GetData16();

  int n_samp = bklen / (sizeof(uint16_t));

  fWaveform.assign(p, p + n_samp);
}

TOUT::TOUT(int bklen, int bktype, const char *name, void *pdata) :
    TGenericData(bklen, bktype, name, pdata) {

  // Data format is simply a list of WORDs
  const uint16_t* p = GetData16();

  int n_samp = bklen / (sizeof(uint16_t));

  fHeader[0] = *p++;
  fHeader[1] = *p++;
  fHeader[2] = *p++;
  fHeader[3] = *p++;

  fWaveform.assign(p, p + n_samp - 4);
}
