#ifndef TEvaluationData_hxx_seen
#define TEvaluationData_hxx_seen

#include <vector>
#include <map>

#include "TGenericData.hxx"


// Decoding of data from ADC0 bank.
class TADC: public TGenericData {
  public:
    TADC(int bklen, int bktype, const char *name, void *pdata);

    int GetNSamples() const {
      return fWaveform.size();
    }

    int GetADCSample(int i) const {
      if (i >= 0 && i < (int) fWaveform.size()) {
        return fWaveform[i];
      }

      return -1;
    }

  private:
    std::vector<uint16_t> fWaveform;
};


// Decoding of data from OUT0 bank.
class TOUT: public TGenericData {
  public:
    TOUT(int bklen, int bktype, const char *name, void *pdata);

    int GetHeaderWord(int i) {
      if (i >= 0 && i <= 3) {
        return fHeader[i];
      } else {
        return -1;
      }
    }

    int GetNSamples() const {
      return fWaveform.size();
    }

    int GetADCSample(int i) const {
      if (i >= 0 && i < (int) fWaveform.size()) {
        return fWaveform[i];
      }

      return -1;
    }

  private:
    uint16_t fHeader[4];
    std::vector<uint16_t> fWaveform;
};

#endif
