#include "TEvaluationHistograms.hxx"
#include "TEvaluationData.hxx"
#include "TH1D.h"
#include "TDirectory.h"
#include <iostream>

using namespace std;

const int numHistosPerGroup = 2;
const int numGroups = 1;

/// Reset the histograms for this canvas
TEvaluationHistograms::TEvaluationHistograms() {
  SetTabName("Evaluation board");
  SetSubTabName("Waveforms");
  SetUpdateOnlyWhenPlotted(false); // So visible on web

  fNanosecsPerSample = 8; //ADC clock runs at 125Mhz on the VX2740 = units of 8 nsecs
  fNSamples = 2052;

  CreateHistograms();
  SetNumberChannelsInGroup(numHistosPerGroup);

  // Rootana has a strict view on how data is organised, which doesn't
  // really match what we want.
  // We'll make a "group" be the data from ADC0 and OUT0.
  // If more channels are added in future (ADC1/OUT1 etc?) then we can
  // add more groups and still keep the ADC/OUT data close together in the
  // rootana interface.
  SetGroupName("Channel");
  SetChannelName("Raw?");

}

void TEvaluationHistograms::CreateHistograms() {
  // check if we already have histograms.
  if (size() != 0) {
    char tname[100];
    sprintf(tname, "Evaluation_%i_%i", 0, 0);

    TH1D *tmp = (TH1D*) gDirectory->Get(tname);
    if (tmp) {
      return;
    }
  }

  int WFLength = fNSamples * fNanosecsPerSample;

  // Otherwise make histograms
  clear();

  for (int group = 0; group < numGroups; group++) {
    for (int hist = 0; hist < numHistosPerGroup; hist++) {

      char name[100];
      char title[100];
      sprintf(name, "Evaluation_%i_%i", group, hist);
      TH1D *otmp = (TH1D*) gDirectory->Get(name);

      if (otmp) {
        delete otmp;
      }

      if (hist == 0) {
        sprintf(title, "Data from ADC%1d bank", group);
      } else {
        sprintf(title, "Data from OUT%1d bank", group);
      }

      TH1D *tmp = new TH1D(name, title, fNSamples, 0, WFLength);
      tmp->SetXTitle("ns");
      tmp->SetYTitle("ADC value");

      push_back(tmp);
    }
  }
}

void TEvaluationHistograms::UpdateHistograms(TDataContainer &dataContainer) {
  char name[100];
  sprintf(name, "ADC0");
  TADC* adc_data = dataContainer.GetEventData<TADC>(name);

  sprintf(name, "OUT0");
  TOUT* out_data = dataContainer.GetEventData<TOUT>(name);

  if (!adc_data || !out_data) {
    // No ADC0/OUT0 bank in this event.
    return;
  }

  // Fill the histograms.
  // Note first visible bin is index 1 (0 is the underflow bin).

  // Histo 0 has the raw data.
  for (int samp = 0; samp < adc_data->GetNSamples(); samp++) {
    GetHistogram(0)->SetBinContent(samp + 1, adc_data->GetADCSample(samp));
  }

  // Histo 1 has the manipulated data.
  for (int samp = 0; samp < out_data->GetNSamples(); samp++) {
    GetHistogram(1)->SetBinContent(samp + 1, out_data->GetADCSample(samp));
  }
}

void TEvaluationHistograms::Reset() {
  // Reset the histograms...
  for (int i = 0; i < size(); i++) {
    for (int bin = 0; bin <= GetHistogram(i)->GetNbinsX(); bin++) {
      GetHistogram(i)->SetBinContent(bin, 0);
    }

    GetHistogram(i)->Reset();
  }
}
