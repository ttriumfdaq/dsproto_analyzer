#ifndef TEvaluationHistograms_hxx
#define TEvaluationHistograms_hxx

#include <string>
#include "THistogramArrayBase.h"

/// Class for making histograms of raw Evaluation waveforms;
/// right now is only for raw data.
class TEvaluationHistograms : public THistogramArrayBase {
public:
  TEvaluationHistograms();
  virtual ~TEvaluationHistograms(){};

  void UpdateHistograms(TDataContainer& dataContainer);

  // Reset the histograms; needed before we re-fill each histo.
  void Reset();

  void CreateHistograms();

  /// Take actions at begin run
  void BeginRun(int transition,int run,int time){
    CreateHistograms();
  }

private:
  int fNanosecsPerSample;
  int fNSamples;
};


#endif


