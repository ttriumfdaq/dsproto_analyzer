#ifndef TVX2740RawData_hxx_seen
#define TVX2740RawData_hxx_seen

#include <vector>
#include <map>

#include "TGenericData.hxx"

struct TVX2740EventHeader {
   uint8_t format;
   uint32_t event_counter;
   uint32_t size_64bit_words; // includes size of 24 byte header
   uint16_t flags;
   uint8_t overlap;
   uint64_t trigger_time;
   uint64_t ch_enable_mask;

   TVX2740EventHeader(const uint64_t* buffer=NULL);

   uint32_t size_bytes();
   uint32_t samples_per_chan();
};


/// Class to store information from a single VX2740 channel.
class TVX2740RawChannel {

public:

  /// constructor
  TVX2740RawChannel(int channel=-1, int num_samples=0) {
    fChannelNumber = channel;
    fWaveform.resize(num_samples);
  }

  int GetChannelNumber() const {
    return fChannelNumber;
  }

  /// Get the ADC sample for a particular bin (for uncompressed data).
  int GetNSamples() const {
    return fWaveform.size();
  }

  /// Get the ADC sample for a particular bin (for uncompressed data).
  int GetADCSample(int i) const {
    if (i >= 0 && i < (int) fWaveform.size())
      return fWaveform[i];

    // otherwise, return error value.
    return -1;
  }

  /// Returns true for objects with no ADC samples or ZLE pulses.
  int IsEmpty() const {
    return fWaveform.size() == 0;
  }

  /// Set ADC sample
  void SetADCSample(uint32_t i, uint16_t sample) {
    fWaveform[i] = sample;
  }

private:

  /// Channel number
  int fChannelNumber;

  std::vector<uint16_t> fWaveform;

};

/// Class to store data from CAEN VX2740, 125MHz FADC.
///
/// This class encapsulates the data from a single board (in a single MIDAS bank).
/// This decoder is for the default version of the firmware.
class TVX2740RawData: public TGenericData {

public:

  /// Constructor
  TVX2740RawData(int bklen, int bktype, const char *name, void *pdata);

  /// Get the number of 32-bit words in bank.
  TVX2740EventHeader GetHeader() const {
    return fHeader;
  }

  /// Get Number of channels in this bank.
  int GetNChannels() const {
    return fMeasurements.size();
  }

  /// Get Channel Data
  TVX2740RawChannel& GetChannelData(int i) {
    return fMeasurements[i];
  }

  void Print();

private:

  TVX2740EventHeader fHeader;

  /// Vector of VX2740 measurements
  std::map<int, TVX2740RawChannel> fMeasurements;

};

#endif
