#include "TVX2740Waveform.hxx"
#include "TVX2740RawData.hxx"
#include "TH1D.h"
#include "TDirectory.h"
#include <iostream>

using namespace std;

const int numberChannelPerModule = 64;

/// Reset the histograms for this canvas
TVX2740Waveform::TVX2740Waveform() {
  fCanvas = NULL;

  SetTabName("VX2740");
  SetSubTabName("Waveforms");
  SetUpdateOnlyWhenPlotted(false); // So visible on web

  SetNanosecsPerSample(8); //ADC clock runs at 125Mhz on the VX2740 = units of 8 nsecs
  SetNumberChannelsInGroup(numberChannelPerModule);

  std::map<int, int> tmp_boards;
  tmp_boards[1] = 1;
  SetNumBoardsPerFrontend(tmp_boards);

}

void TVX2740Waveform::SetNumBoardsPerFrontend(std::map<int, int> num_boards_per_fe) {
  fTotNumBoards = 0;
  fNumBoardsPerFE = num_boards_per_fe;
  fFEAndBoardToHistoOrder.clear();

  for (auto it : num_boards_per_fe) {
    int fe = it.first;

    for (int i = 0; i < it.second; i++) {
      fFEAndBoardToHistoOrder[std::make_pair(fe, i)] = fTotNumBoards;
      fTotNumBoards++;
    }
  }

  printf("Total number of VX2740 boards for this run is %d\n", fTotNumBoards);

  if (fTotNumBoards == 0) {
    fTotNumBoards = 1;
  }

  ForceCreateHistograms();

  if (fCanvas) {
    fCanvas->SetGroupName("Board");
    fCanvas->SetChannelName("Channel");
    fCanvas->HandleChangedNumberOfHistos();
  }
}

void TVX2740Waveform::ForceCreateHistograms() {
  // Delete existing histograms
  for (int j = 0; j < fTotNumBoards; j++) { // loop over modules
    for (int i = 0; i < numberChannelPerModule; i++) { // loop over channels
      char tname[100];
      sprintf(tname, "VX2740_%i_%i", j, i);
      TH1D *tmp = (TH1D*) gDirectory->Get(tname);
      delete tmp;
    }
  }
  clear();

  // Then re-create them.
  CreateHistograms();
}

void TVX2740Waveform::CreateHistograms() {
  // check if we already have histograms.
  if (size() != 0) {
    char tname[100];
    sprintf(tname, "VX2740_%i_%i", 0, 0);

    TH1D *tmp = (TH1D*) gDirectory->Get(tname);
    if (tmp)
      return;
  }

  int defaultNSamples = 1000;
  int WFLength = defaultNSamples * nanosecsPerSample; // Need a better way of detecting this...

  // Otherwise make histograms
  clear();
  int j = 0;

  for (auto fe_boards : fNumBoardsPerFE) { // loop over frontends
    int fe_idx = fe_boards.first;

    for (int board_idx = 0; board_idx < fe_boards.second; board_idx++) {
      for (int i = 0; i < numberChannelPerModule; i++) { // loop over channels

        char name[100];
        char title[100];
        sprintf(name, "VX2740_%i_%i", j, i);
        TH1D *otmp = (TH1D*) gDirectory->Get(name);
        if (otmp) {
          delete otmp;
        }

        sprintf(title, "VX2740 Waveform for frontend=%i, board=%i, channel=%i", fe_idx, board_idx, i);

        TH1D *tmp = new TH1D(name, title, defaultNSamples, 0, WFLength);
        tmp->SetXTitle("ns");
        tmp->SetYTitle("ADC value");

        push_back(tmp);
        j++;
      }
    }
  }
}

void TVX2740Waveform::UpdateHistograms(TDataContainer &dataContainer) {
  int fe_idx = dataContainer.GetMidasEvent().GetTriggerMask();

  for (int board_idx = 0; board_idx < fNumBoardsPerFE[fe_idx]; board_idx++) {  // loop over boards in this frontend
    char name[100];
    sprintf(name, "D%03i", board_idx); // Check for module-specific data
    TVX2740RawData *VX2740 = dataContainer.GetEventData<TVX2740RawData>(name);

    int base_idx = fFEAndBoardToHistoOrder[std::make_pair(fe_idx, board_idx)] * numberChannelPerModule;

    if (VX2740) {
      if (VX2740->GetHeader().format != 0x10) {
        // Not scope-data format
        continue;
      }

      for (int chan = 0; chan < numberChannelPerModule; chan++) { // loop over channels
        int index = chan + base_idx;

        if (!(VX2740->GetHeader().ch_enable_mask & ((uint64_t)1 << chan))) {
          continue;
        }

        TVX2740RawChannel& channelData = VX2740->GetChannelData(chan);

        if (channelData.GetNSamples() != GetHistogram(index)->GetNbinsX()) {
          GetHistogram(index)->SetBins(channelData.GetNSamples(), 0, channelData.GetNSamples() * nanosecsPerSample);
        }

        for (int samp = 0; samp < channelData.GetNSamples(); samp++) {
          double adc = channelData.GetADCSample(samp);
          GetHistogram(index)->SetBinContent(samp + 1, adc);
        }
      }
    } else {
      printf("Didn't see bank %s\n", name);
    }
  }
}

void TVX2740Waveform::Reset() {

  for (int j = 0; j < fTotNumBoards; j++) {        // loop over modules

    for (int i = 0; i < numberChannelPerModule; i++) { // loop over channels
      int index = i + j * numberChannelPerModule;

      // Reset the histogram...
      for (int ib = 0; ib < GetHistogram(index)->GetNbinsX(); ib++) {
        GetHistogram(index)->SetBinContent(ib, 0);
      }

      GetHistogram(index)->Reset();

    }
  }
}
