#ifndef TVX2740Waveform_hxx
#define TVX2740Waveform_hxx

#include <string>
#include "TDynamicNumHistosCanvas.hxx"
#include "THistogramArrayBase.h"

/// Class for making histograms of raw VX2740 waveforms;
/// right now is only for raw data.
class TVX2740Waveform : public THistogramArrayBase {
public:
  TVX2740Waveform();
  virtual ~TVX2740Waveform(){};

  void UpdateHistograms(TDataContainer& dataContainer) override;

  /// Getters/setters
  int GetNsecsPerSample() { return nanosecsPerSample; }
  void SetNanosecsPerSample(int nsecsPerSample) { this->nanosecsPerSample = nsecsPerSample; }

  // Reset the histograms; needed before we re-fill each histo.
  void Reset();

  void SetNumBoardsPerFrontend(std::map<int, int> num_boards_per_fe);

  TCanvasHandleBase* CreateCanvas() override {
    fCanvas = new TDynamicNumHistosCanvas(this, GetSubTabName());
    return fCanvas;
  }

  void CreateHistograms() override;
  void ForceCreateHistograms();


private:
  int nanosecsPerSample;
  std::map<int, int> fNumBoardsPerFE;
  std::map<std::pair<int, int>, int> fFEAndBoardToHistoOrder;
  int fTotNumBoards;

  TDynamicNumHistosCanvas* fCanvas;
};


#endif


