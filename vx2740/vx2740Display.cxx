#include <stdio.h>
#include <iostream>

#include "TRootanaDisplay.hxx"
#include "TFancyHistogramCanvas.hxx"

#include "TVX2740AnaManager.hxx"

class VX2740Loop: public TRootanaDisplay {

public:

  // An analysis manager.  Define and fill histograms in
  // analysis manager.
  TVX2740AnaManager *anaManager;

  VX2740Loop() {
    SetOutputFilename("vx2740_display");
    DisableRootOutput(true);
    anaManager = new TVX2740AnaManager();
  }

  void AddAllCanvases() {
    // Set up tabbed canvases

    // Set up all the listed canvases from the AnaManager list of THistogramArrayBases
    std::vector<THistogramArrayBase*> histos = anaManager->GetHistograms();

    for (unsigned int i = 0; i < histos.size(); i++) {
      TCanvasHandleBase* canvas = histos[i]->CreateCanvas();

      if (canvas) {
        AddSingleCanvas(canvas, histos[i]->GetTabName());
      }
    }

    SetDisplayName("VX2740 Display");
  }

  virtual ~VX2740Loop() {};

  void BeginRun(int transition,int run,int time) {
    anaManager->BeginRun(transition, run, time, GetODB());
  }

  void EndRun(int transition,int run,int time) {
    anaManager->EndRun(transition, run, time);
  }

  void ResetHistograms(){}

  void UpdateHistograms(TDataContainer& dataContainer){
    // Update the cumulative histograms here
    anaManager->ProcessMidasEvent(dataContainer);
  }

  void PlotCanvas(TDataContainer& dataContainer){
    // Update the transient (per-event) histograms here.
    // saves CPU to not update them always when not being used.
    anaManager->UpdateTransientPlots(dataContainer);
  }
};


int main(int argc, char *argv[])
{
  VX2740Loop::CreateSingleton<VX2740Loop>();
  return VX2740Loop::Get().ExecuteLoop(argc, argv);
}

