#include <stdio.h>
#include <iostream>
#include <time.h>

#include "TRootanaEventLoop.hxx"

#include "TVX2740AnaManager.hxx"

class VX2740WebLoop: public TRootanaEventLoop {

public:

  // An analysis manager.  Define and fill histograms in
  // analysis manager.
  TVX2740AnaManager *anaManager;

  VX2740WebLoop() {
    SetOnlineName("jsroot_server");
    DisableRootOutput(true);
    anaManager = new TVX2740AnaManager();
    UseBatchMode();
    gettimeofday(&LastUpdateTime, NULL);
  }

  void Initialize() {
#ifdef HAVE_THTTP_SERVER
    std::cout << "Using THttpServer in read/write mode" << std::endl;
    SetTHttpServerReadWrite();
#endif
  }

  virtual ~VX2740WebLoop() {};

  void BeginRun(int transition,int run,int time) {
    anaManager->BeginRun(transition, run, time, GetODB());
  }

  void EndRun(int transition,int run,int time) {
    anaManager->EndRun(transition, run, time);
  }

  struct timeval LastUpdateTime;

  bool ProcessMidasEvent(TDataContainer& dataContainer){
    anaManager->ProcessMidasEvent(dataContainer);

    // Only update the transient histograms (like waveforms or event displays) every second.
    // Otherwise hammers CPU for no reason.
    struct timeval nowTime;
    gettimeofday(&nowTime, NULL);
    double dtime = nowTime.tv_sec - LastUpdateTime.tv_sec + (nowTime.tv_usec - LastUpdateTime.tv_usec)/1000000.0;
    if(dtime > 1.0){
      anaManager->UpdateTransientPlots(dataContainer);
      LastUpdateTime = nowTime;
    }

    return true;
  }
};


int main(int argc, char *argv[])
{
  VX2740WebLoop::CreateSingleton<VX2740WebLoop>();
  return VX2740WebLoop::Get().ExecuteLoop(argc, argv);
}

